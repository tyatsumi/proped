Kantan Properties Editor
========================

Eclipse plugin for editing Java properties files, automatically converts non-ASCII characters to Unicode escape sequences.

This plugin is a fork from Simple Properties Editor created by Piotr Wolny.

Kantan means Simple and Easy in Japanese.

Installation
------------

<a href="https://marketplace.eclipse.org/marketplace-client-intro?mpc_install=5638208" title="Drag and drop into a running Eclipse workspace to install Kantan Properties Editor">
  <img src="https://marketplace.eclipse.org/sites/all/modules/custom/marketplace/images/installbutton.png"/>
</a>

Drag and drop above button into a running Eclipse workspace or click "Help -> Install new software" and paste update site URL:<br>
https://tyatsumi.gitlab.io/proped/

License
-------

<a href="https://tyatsumi.gitlab.io/proped/LICENSE.txt">MIT</a>
